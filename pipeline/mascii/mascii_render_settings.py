#!/usr/bin/python

import os
import re
import shutil

input_path = os.path.normpath("D:/Dropbox/ChaosWarBears/_chaoswarbears/release/shots/")
# input_path = os.path.normpath("C:/Users/justin/Desktop")

maya_ascii_files = [os.path.join(input_path, f) for f in os.listdir(input_path) if f.endswith('.ma')]

for maf in maya_ascii_files:
    if not os.path.exists(maf):
        raise RuntimeError("Path {0} does not exist.".format(maf))


"""
select -ne :defaultRenderGlobals;
    setAttr ".ren" -type "string" "mentalRay";
    setAttr ".outf" 32;
    setAttr ".imfkey" -type "string" "png";  # image format key (extension)
    setAttr ".an" yes;
    setAttr ".fs" 20;  # frame start
    setAttr ".ef" 525;  # end frame
    setAttr ".ep" 4;  # frame padding
    setAttr ".bfs" 1;  # by frame step (can exclude if 1)
    setAttr ".pff" yes;
    setAttr ".peie" 2;  # Frame/Animation extension (set to name_#.ext)
    setAttr ".ifp" -type "string" "<Scene>/<Camera>/<Scene>";
select -ne :defaultResolution;
    setAttr ".w" 1280;  # render resolution width
    setAttr ".h" 720;  # render resolution height
    setAttr ".pa" 1;
    setAttr ".dar" 1.7769999504089355;
"""

render_engine = "mentalRay"
fileformat = "png"
frame_start = 1
frame_end = 120
frame_padding = 4
frame_extension_template = 2  # name_#.ext
image_file_pattern = "<Scene>/<Camera>/<Scene>"

default_render_globals = {
    ".ren": "-type \"string\" \"{0}\"".format(render_engine),
    ".imfkey": "-type \"string\" \"{0}\"".format(fileformat),  # image format key (extension)
    # ".fs": frame_start,  # frame start
    # ".ef": frame_end,  # end frame
    ".ep": frame_padding,  # frame padding
    ".peie": frame_extension_template,  # Frame/Animation extension (set to name_#.ext)
    ".ifp": "-type \"string\" \"{0}\"".format(image_file_pattern)
}


def get_framerange(f):

    line = next(f)

    while not "playbackOptions" in line:
        line = next(f)

    playback_options = re.search(r"playbackOptions[\s\-\w\d]+", line)
    parameters = re.findall(r"-\w+\s\d+", playback_options.group())

    for p in parameters:
        if "ast" in p:
            default_render_globals[".fs"] = int(re.search(r"\d+", p).group())
        if "aet" in p:
            default_render_globals[".ef"] = int(re.search(r"\d+", p).group())

    for g in default_render_globals:
        print(g, default_render_globals[g])


def write_ascii_values(f, settings, tmp_f=None):

    # Make a local copy of our dictionary
    settings = dict(settings)

    # Sets the temp file to the current file if none was specificed (overwrite mode)
    if not tmp_f:
        tmp_f = f

    # Scan to the default render globals
    line = next(f)  # get the first line of the file, we'll be skipping through

    while not "defaultRenderGlobals" in line:
        tmp_f.write(line)
        line = next(f)

    tmp_f.write(line)  # actually writes the line containing "defaultRenderGlobals"

    # Write the default render globals
    for i, line in enumerate(f, 1):

        if "select" in line:
            next_block = line
            break

        found_key = next((key for key in settings.keys() if key in line), None)

        if found_key:
            print("Replacing value of {key} on line {num} with {val}"
                  .format(key=found_key, num=i, val=settings[found_key]))

            tmp_f.write("\tsetAttr \"{key}\" {value};\n"
                        .format(key=found_key, value=settings[found_key]))

            settings.pop(found_key)

        else:
            tmp_f.write(line)

    # Are there still settings we've yet to write? If so, write them!
    if settings:
        for key in settings.keys():
            tmp_f.write("\tsetAttr \"{key}\" {value};\n".format(key=key,
                                                                   value=settings[key]))
        print("Wrote {num} settings not found in file\n\n".format(num=len(settings)))

    # write out the select block we read in earlier
    tmp_f.write(next_block)

    # Finish writing the rest of the lines
    for line in f:
        tmp_f.write(line)


def fix_files(ascii_files, replace=True, tmp_suffix=".tmp"):

    for maf in ascii_files:

        with open(maf, "r") as f:
            get_framerange(f)

        (filename, ext) = os.path.splitext(os.path.basename(maf))

        tmp_filepath = ''.join([input_path, filename, ext, tmp_suffix])
        print("Opening tmp file at {0}".format(tmp_filepath))

        with open(maf) as f:
            with open(tmp_filepath, 'w') as tmp:
                write_ascii_values(f, default_render_globals, tmp)

        if replace:
            shutil.copy2(tmp_filepath, maf)


if __name__ == "__main__":
    # Test editing render settings with a temporary file
    #fix_with_temp([maya_ascii_files[0]])

    # Test searching for playback settings
    fix_files(maya_ascii_files)
