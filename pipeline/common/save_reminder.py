from ..maya.timertools import MayaIntervalTimer
from ..maya.guitools import MayaHUD
import pymel.core as pm

# 10 minutes * 60 seconds / minute
#timer = MayaIntervalTimer(10 * 60)
timer = MayaIntervalTimer(5*60)


def new_interval_after_save():
    timer.check_interval()
    hud.refresh()


def update(*args):
    delta = timer.delta()
    interval = timer.interval

    if delta >= interval:
        if pm.cmds.file(query=True, amf=True):
            pm.scriptJob(runOnce=True, event=["SceneSaved", new_interval_after_save])
            return "SAVE NOW!"
        else:
            new_interval_after_save()
            return "0s ago"

    if delta < 60:
        return "{0}s ago".format(int(delta))
    else:
        return "{0}m {1}s ago".format(int(delta // 60), int(delta % 60))


def HUDSettings():

    settings = {
        "section": 9,
        "blockSize": 'medium',
        "label": u"Last saved (apprx.)",
        "labelFontSize": 'small',
        "command": update,
        "event": 'SelectionChanged',
        "nodeChanges": 'attributeChange'
    }

    return settings


hud = MayaHUD("HUDSaveReminder", HUDSettings())
hud.buildUI()
