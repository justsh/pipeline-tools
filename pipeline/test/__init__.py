import sys
import os


loaded_packages = []


def load_local_package(path):
    if not path in sys.path:
        sys.path.insert(0, path)


def unload_local_package(path):
    sys.path.remove(path)


def promptLoadLocal(*args):
    import pymel.core as pm

    workspace_path = pm.Workspace().getPath()
    result = pm.fileDialog2(fileMode=3,
                            dir=workspace_path,
                            okCaption="Select")[0]

    if result:
        loaded_packages.append(os.path.normpath(result))
        load_local_package(os.path.normpath(result))
    else:
        pm.displayError("Could not load package at "
                        "{0}".format(result))


def promptUnloadLocal(*args):
    import pymel.core as pm

    def __layoutDialog(*args):

        def __unload(menu, *args):
            selected = pm.optionMenu(menu, query=True, value=True)
            if selected:
                try:
                    unload_local_package(selected)
                    loaded_packages.remove(selected)
                except ValueError:
                    pm.displayError("Path \"{0}\" is already unloaded".format(selected))
                else:
                    pm.displayInfo("Unloaded {0} successfully".format(selected))

        dismiss = lambda *args: pm.layoutDialog(dismiss="Close")

        form = pm.setParent(query=True)
        pm.formLayout(form, edit=True, width=480)

        col = pm.columnLayout(adjustableColumn=True,
                              columnAttach=('both', 10),
                              rowSpacing=10)

        pm.text(label="Unload local testing paths from sys.path", font="boldLabelFont", align="left")
        pm.separator(style="none", height=8)

        options = pm.optionMenu(label='Recent')
        pm.separator(style="none", height=8)

        for path in loaded_packages:
            pm.menuItem(path, label=os.path.normpath(path))

        cb = pm.Callback(__unload, options)

        pm.rowLayout(numberOfColumns=2,
                     #adjustableColumn=2,
                     columnAttach=[(1, 'right', 5),
                                   (2, 'both', 5)])

        pm.button("Close", command=dismiss, width=120)
        pm.button("Unload Selected", command=cb, width=120)

        pm.formLayout(form,
                      edit=True,
                      attachForm=[(col, 'top', 15),
                                  (col, 'left', 5),
                                  (col, 'right', 5),
                                  (col, 'bottom', 15)],
                      )

    if loaded_packages:
        pm.layoutDialog(title="Remove Local Testing Path", ui=__layoutDialog)
    else:
        pm.confirmDialog(title="Remove Local Testing Path",
                         message="No local testing paths were found registered",
                         button=["Dismiss"])
