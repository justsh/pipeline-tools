import pymel.core as pm
from collections import namedtuple


RL = namedtuple('RenderLayerDict', ['name', 'preset', 'passes', 'allnodes', 'constructor'])


def create_render_pass(pass_name, pass_type):
    """Creates a single render pass"""

    if pm.objExists(pass_name):
        pm.delete(pass_name)

    pm.createNode("renderPass", name=pass_name)
    pm.setRenderPassType(pass_name, type=pass_type)
    pm.setAttr(pass_name + ".passGroupName", "Illumination")


def create_render_passes(*args):
    """Creates passes from an arbitrary number of pass dictionaries"""

    for pass_dict in args:
        for pass_name, pass_type in pass_dict.iteritems():
            create_render_pass(pass_name, pass_type)


def create_render_layer(name, selection, preset=None, g=False):
    """Creates a single render layer"""

    if not pm.objExists(name):
        pm.createRenderLayer(selection, name=name, makeCurrent=True, g=g)
        if preset:
            melcmd = "renderLayerBuiltinPreset {preset} {name}"
            pm.mel.eval(melcmd.format(preset=preset, name=name))
    else:
        print("Node {0} already exists. Skipping creation".format(name))


def create_render_layers(rlayers, selection=None):
    """Creates multiple render layers at once"""

    if not selection:
        pm.mel.mprint("Creating empty render layers")

    for name, rldict in rlayers.items():
        create_render_layer(name,
                            selection,
                            getattr(rldict, "preset"),
                            getattr(rldict, "allnodes"))


def associate_pass_to_rl(pass_name, render_layer):
    pm.connectAttr(render_layer + ".renderPass", pass_name + ".owner", nextAvailable=True)


def associate_passes(rlayers):

    for rlname, rldict in rlayers.iteritems():
        passes_dict = getattr(rldict, "passes")

        if passes_dict:
            for pass_name in passes_dict:
                associate_pass_to_rl(pass_name, rlname)
