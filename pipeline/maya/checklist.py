import pymel.core as pm
from . import mayautils
from .mayautils import mprint


def is_empty():
    """ Naive empty scene check"""
    tplvl = pm.ls(assemblies=True)

    return not mayautils.rm_default_cams(tplvl)


def is_saved():
    #TODO:
    # Check if a given object is on the asset side of the pipeline
    filepath = pm.sceneName()

    if not filepath:
        filepath = mayautils.save_untitled()

        if not filepath:
            mprint("File cannot be published until saved")
            return False

    return True


def is_grouped():

    # Correctly identified
    #   Geometry
    #   Groups
    #   Parents

    group_count = 0
    properly_grouped = True

    tplvl = pm.ls(assemblies=True)

    tplvl = mayautils.rm_default_cams(tplvl)

    for assembly in tplvl:
        # Groups are (I believe) the only node type that cannot, by definition
        # have a shape node attached to them (only a transform)
        shapes = pm.listRelatives(assembly, shapes=True)

        if shapes or pm.nodeType(assembly) == "joint":
            # Current node is not a group"
            pm.displayError("Node \"{0}\" is not grouped".format(assembly))
            properly_grouped = False
        else:
            # Current node is a group"
            group_count += 1

    if group_count > 1:
        pm.displayWarning("Multiple top-level groups exist. Please keep the hierarchy clean")

    return properly_grouped


def shader_check(shaders=[]):

    #TODO:
    # Take input of a list of shaders

    # See if any objects are still using lambert1
    pm.select(clear=True)
    defaultLambert = pm.PyNode("lambert1")

    # This will select all objects currently using the given shader
    pm.hyperShade(objects=defaultLambert)
    sel = pm.ls(sl=True)

    if sel:
        print("There are currently {count} objects using the default shader {name}"
              .format(count=len(sel), name=defaultLambert.name()))
        return False

    return True


def default_names_check():

    default_names = ["pCube", "pCylinder", "pPlane"]

    for name in default_names:
        lst = pm.ls(name+'*')
        if lst:
            print("There are {0} objects using the name {1}"
                  .format(len(lst), name))
            return False

    return True


def has_history(remove=True):

    allShapes = pm.ls(dag=True, shapes=True)

    count = 0
    for shape in allShapes:

        objs_history = None

        try:
            objs_history = pm.listHistory(pruneDagObjects=True, leaf=True, interestLevel=2)
        except RuntimeError:
            print("Found no items to list the history for!")

        if objs_history:
            if remove:
                pm.delete(constructionHistory=True)
            else:
                # warn that object still has history
                count += 1

    print("There are {0} objects with undeleted history".format(count))
    return (count != 0)
