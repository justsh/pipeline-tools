"""
Save and Load Influence weights per vertex per influence

References:
    http://www.charactersetup.com/tutorial_skinWeights.html
    http://www.mail-archive.com/python_inside_maya@googlegroups.com/msg00883.html

"""

from collections import namedtuple
from .mayautils import mprint
import pymel.api as api
import pymel.core as pm
import json


WeightedVertex = namedtuple("WeightedVertex",
                            "vertex inf_id inf_name weight")


# TODO: Implement position based weight mapping
# The commented code at the bottom marks some of the pitfalls
PositionWeight = namedtuple("PositionWeight",
                            "position, inf_name, weight")


def get_weights(geometry):

    # Get the transform we want to back up skin clusters for
    geoTransform = pm.ls(geometry, transforms=True)[0]

    # Find the skin cluster within the transform's children and dag nodes
    geoSkinCluster = geoTransform.history(type="skinCluster")[0]

    # Save a list of WeightedVertex objects
    weighted_vertices = get_skin_weights(geoSkinCluster.name())

    mprint(len(weighted_vertices), "weights found for", geoSkinCluster.name())

    return weighted_vertices


def save_weights(weighted_vertices, filepath):

    # Dump the objects out to file
    with open(filepath, "w+") as f:
        json.dump(weighted_vertices, f)

    mprint("Saved", len(weighted_vertices), "weights to", filepath)


def load_weights(filepath):
    """ Load the json file of saved weighted vertices """

    # make a list to hold the WeightedVertex objects
    weighted_vertices = []

    # Load in the json data and unpack each list into
    # the WeightedVertex constructor
    with open(filepath, "r") as f:
        json_load = json.load(f)

        for wv in json_load:
            weighted_vertices.append(WeightedVertex(*wv))

    mprint("Found", len(weighted_vertices), "weights at", filepath)

    return weighted_vertices


def set_weights(geometry, verts, method=["index"]):

    # Get the transform we want to back up skin clusters for
    geoTransform = pm.ls(geometry, transforms=True)[0]

    # Find the skin cluster within the transform's children and dag nodes
    geoSkinCluster = geoTransform.history(type="skinCluster")[0]

    # The transform could have multiple shapes (like a *ShapeOriginal)
    geoShape = geoTransform.getShapes()[0]

    # unlock influences used by skinCluster
    for v in verts:
        pm.setAttr("{0}.liw".format(v.inf_name))

    # normalize needs to be turned off for the prune to work
    normalized = geoSkinCluster.normalizeWeights

    if normalized:
        geoSkinCluster.normalizeWeights = False

    try:
        pm.skinPercent(geoSkinCluster,
                       geoShape,
                       normalize=False,
                       pruneWeights=100)
    except RuntimeError as re:
        print(re)

    # restore normalize setting
    if normalized:
        geoSkinCluster.normalizeWeights = normalized

    if "index" in method:
        # Set the weights per vertex using the weights variable
        # Using setAttr instead of the API allows us to utilize builtin Undo
        # even though it might be a touch slower
        for wv in verts:
            weightListAttr = '{0}.weightList[{1}]'.format(
                geoSkinCluster.name(), wv.vertex)

            weightAttr = '.weights[{0}]'.format(wv.inf_id)
            pm.setAttr(weightListAttr + weightAttr, wv.weight)

    if "position" in method:

        shape_verts = geoShape.vtx[:]

        for pv in verts:

            to_skin = []

            for v in shape_verts:
                pass

        pass

    mprint("Set", len(verts), "weights on", geoTransform.name(),
           "cluster", geoSkinCluster.name())


def get_skin_weights(skin_cluster_name, method=None, tolerance=1e-4):

    if method is None:
        method = ["index"]

    node = api.toMObject(skin_cluster_name)
    skinClusterNode = api.MFnSkinCluster(node)

    # get the number of influences that affect the skinCluster
    influences = api.MDagPathArray()
    # influenceObjects will return the the length
    skinClusterNode.influenceObjects(influences)

    # get dagPath for the skinCluster
    # skinDagPath = api.toMDagPath(skinClusterNode)

    # here is a list , of all the joint-names and influence objects , inside
    # the skinCluster
    influence_names = []

    for i in xrange(influences.length()):
        inf = influences[i]
        inf_name = inf.partialPathName()
        # inf_path = inf.fullPathName()
        influence_names.append(inf_name)

    # Stores weight information per vertex index
    if "index" in method:

        vertex_weights = []

        weightListPlug = skinClusterNode.findPlug('weightList')
        weightListAttr = weightListPlug.attribute()

        weightPlug = skinClusterNode.findPlug('weights')
        weightAttr = weightPlug.attribute()

        weightInfluenceIds = api.MIntArray()

        for vertex_id in xrange(weightListPlug.numElements()):

            # tell the weights attribute which vertex id it represents
            weightPlug.selectAncestorLogicalIndex(vertex_id, weightListAttr)

            # get the indices of all non-zero weights for this vert.
            # copy the plug
            weightPlug.getExistingArrayAttributeIndices(weightInfluenceIds)
            infPlug = api.MPlug(weightPlug)

            for inf_id in weightInfluenceIds:

                # tell the infPlug it represents the current influence id
                infPlug.selectAncestorLogicalIndex(inf_id, weightAttr)

                # add this influence and its weight to this verts weights
                vertex_weights.append(
                    WeightedVertex(vertex_id,
                                   inf_id,
                                   influence_names[inf_id],
                                   infPlug.asDouble()))

        return vertex_weights

    # Store positions of vertices along with a rig name
    # Not the preferred method, use vertex index when you can
    if "position" in method:

        space = api.MSpace.kWorld
        position_weights = []

        for i in xrange(influences.length()):
            influenceDagPath = api.toMDagPath(influences[i])
            vertices = api.MSelectionList()
            weights = api.MDoubleArray()

            skinClusterNode.getPointsAffectedByInfluence(influenceDagPath,
                                                         vertices,
                                                         weights)

            for n in xrange(vertices.length()):
                meshDag = api.MDagPath()
                component = api.MObject()
                vertices.getDagPath(n, meshDag, component)

                mesh = api.MFnMesh(meshDag)

                indexedComponent = api.MFnSingleIndexedComponent(component)
                indices = api.MIntArray()
                indexedComponent.getElements(indices)

                vindex_weights = ((i, w) for i, w in zip(indices, weights)
                                  if w > tolerance)

                for vindex, weight in vindex_weights:
                    point = api.MPoint()
                    mesh.getPoint(vindex, point, space)

                    point.cartesianize()
                    p = (point.x, point.y, point.z)

                    position_weights.append(
                        PositionWeight(p, influenceDagPath.partialPathName(),
                                       weight))

        return position_weights


"""
# For every geometry, save the weights.
    geoShape = geoTransform.getShapes()[0]

    if geoShape.nodeType() == "mesh":
        points = geoShape.vtx.indices()
    elif geoShape.nodeType() == "nurbsSurface":
        points = geoShape.cv.indices()

    # TODO: Open the weights file.
    weight_file = open(filepath)

    # Get the geometry's points.
    skinnedJoints = geoSkinCluster.getInfluence()
    joints = None

    for joint in skinnedJoints:
        joints.append(joint.name().encode("ascii", "ignore"))

    weight_file.write("// List of joints: " + ' '.join(joints) + '\n')

    # Set the Maya progress window.
    amountStep = 100.0 / len(points)
    amount = 0
    #mel.eval('progressWindow -progress ' + str(int(amount)) + ' -status \"Starting...\"')
    pm.text("swtProgressWindowText", edit=True, label="Starting... ")
    pm.progressBar("swtProgressBar", edit=True, progress=str(int(amount)))

    # For every point, get the weighting info.
    for point in points:
        # Get the influencing joints list.
        skinJoints = pm.skinPercent(point,
                                    query=True,
                                    transform=geoSkinCluster)

        if skinJoints is not None:
            # And what are their skinPercent weights?
            weights = []
            for joint in skinJoints:
                weights.append(pm.skinPercent(geoSkinCluster,
                                              point,
                                              transform=joint,
                                              query=True))

            if "Number" in methods:

                # Write to the files.
                lines = []

                lines.append("skinPercent")
                for c in xrange(0, len(skinJoints)):
                    lines.append("-tv {joint} {weight}".format(
                        joint=skinJoints[c], weight=weights[c]))

                # Note here we print the skinCluster as a variable ($skincl), that way we're not tied to a specific skinCluster name when we re-apply the weights.
                lines.append("$skincl {0};\n".format(point))
                weight_file.write(' '.join(lines))

            if "Position" in methods:
                weight_file.write(point + "\n")
                point_pos = []
                if spaceChoice == "worldSpace":
                    point_pos = pm.pointPosition(point, world=True)
                else:
                    point_pos = pm.pointPosition(point, local=True)

                weight_file.write("{0} {1} {2}\n".format(*point_pos))

                joints_as_string = []
                for c in xrange(0, len(skinJoints)):
                    joints_as_string.append("-tv {joint} {weight}".format(
                        joint=skinJoints[c], weight=weights[c]))
                weight_file.write(' '.join(joints_as_string) + "\n")

        else:
            print "Could not find any joint influence for point " + point

        # Update the Maya progress window.
        amount += amountStep
        #mel.eval('progressWindow -edit -progress ' + str(int(amount)) + ' -status \"' + point + '\"')
        pm.text("swtProgressWindowText", edit=True, label=point)
        pm.progressBar("swtProgressBar", edit=True, progress=str(int(amount)))

    print skinJoints
"""
