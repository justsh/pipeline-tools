import pymel.core as pm


class MayaTimer(object):
    """An small class to help keep time in Maya"""

    def __init__(self, start_at=0.0):
        self.init_time = pm.timerX() - start_at

    def elapsed(self):
        return pm.timerX(startTime=self.init_time)

    def reset(self, iterations=True):
        self.init_time = pm.timerX()


class MayaIntervalTimer(MayaTimer):
    """A timer for Maya that supports intervals"""

    def __init__(self, interval, ignore_overtime=True):
        MayaTimer.__init__(self)

        self.ignore_overtime = ignore_overtime

        self.interval = interval
        self.next_interval = self.start_interval()
        self.iterations = 0

        if interval <= 0:
            raise ValueError("Interval must be a positive value in seconds")

    def reset(self):
        self.init_time = pm.timerX()
        self.next_interval = self.start_interval()
        self.iterations = 0

    def delta(self):
        return self.interval - (self.next_interval - self.elapsed())

    def check_interval(self):
        next_interval = self.next_interval
        elapsed = self.elapsed()

        if self.delta() >= 0:
            spill = elapsed - next_interval

            self.next_interval = self.start_interval()

            if self.ignore_overtime:
                self.iterations += 1
            else:
                self.iterations += spill // self.interval

            return True

        return False

    def start_interval(self):
        return self.elapsed() + self.interval
