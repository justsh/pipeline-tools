import pymel.core as pm
import os

# scope
mi_CACHE_ASSIGNED_TEXTURES = 1
mi_CACHE_ALL_TEXTURES = 2

# mode
mi_USE_DEFAULT_CACHE = 0
mi_USE_CUSTOM_CACHE = 1

# location
mi_CACHE_LOCATION = os.path.join(pm.Workspace().getPath(),
                                 "sourceimages",
                                 "cache")


LAST_USER_SETTINGS = []


def mi_set_texture_opt_settings(cache=True,
                                scope=mi_CACHE_ASSIGNED_TEXTURES,
                                mode=mi_USE_DEFAULT_CACHE,
                                cdir=mi_CACHE_LOCATION):

    global LAST_USER_SETTINGS

    print("loading MentalRay Settings for ChaosWarBears")

    old_cache = pm.optionVar["miCacheOptimFileTextures"]
    old_scope = pm.optionVar["miFileTextureCacheConversionScope"]
    old_mode = pm.optionVar["miFileTextureCacheStorageMode"]
    old_cdir = pm.optionVar["miFileTextureCacheLocation"]

    pm.optionVar["miCacheOptimFileTextures"] = cache
    pm.optionVar["miFileTextureCacheConversionScope"] = scope
    pm.optionVar["miFileTextureCacheStorageMode"] = mode
    pm.optionVar["miFileTextureCacheLocation"] = cdir

    LAST_USER_SETTINGS = [old_cache, old_scope, old_mode, old_cdir]

    return LAST_USER_SETTINGS


def main():
    if not pm.pluginInfo("Mayatomr", query=True, loaded=True):
        try:
            pm.loadPlugin("Mayatomr", quiet=True)
        except:
            # Because for some reason, quiet is not actually quiet
            pass

main()
