import pymel.core as pm


rpanels = pm.getPanel(scriptType="renderWindowPanel")
if "renderView" in rpanels:
    print("Panel renderView already exists")
else:
    for panel in rpanels:
        if panel == "renderWindowPanel1":
            pm.deleteUI("renderWindowPanel1")

    renderPanel = pm.scriptedPanel("renderView",
                                   type="renderWindowPanel",
                                   unParent=True)
    pm.scriptedPanel(renderPanel,
                     edit=True,
                     label=pm.mel.interToUI(renderPanel))
