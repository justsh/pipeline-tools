'''
Created on Feb 13, 2013

@author: Justin Sheehy
'''

import pymel.core as pm


MAYA_CAMERAS = pm.ls(["persp", "top", "front", "side"])


def mprint(*args):
    print_args = ("//",) + args + ("//\n",)
    pm.mel.mprint(*print_args)


def save_untitled():
    result = pm.confirmBox("Untitled New File",
                           "The current file appears to have "
                           "never been saved. Would you like "
                           "to save it now?\n\n"
                           "Clicking \"yes\" will open the default "
                           "\"Save\" dialog.", no='Cancel')

    if not result:
        mprint("Save cancelled by user")
        return None

    fileFilters = ';;'.join(["Maya Files (*.ma *.mb)",
                             "Maya ASCII (*.ma)",
                             "Maya Binary (*.mb)",
                             "All Files (*.*)"])

    result = pm.fileDialog2(caption="Save As",
                            fileFilter=fileFilters,
                            selectFileFilter="Maya ASCII")[0]

    pm.saveAs(result)
    return result


def set_scene_units():
    if not pm.currentUnit(q=True, linear=True) == "ft":
        pm.currentUnit(linear="foot")
        pm.grid(size=12, sp=5, d=5)


def rm_default_cams(object_list):
    for cam in MAYA_CAMERAS:
        if cam in object_list:
            object_list.remove(cam)
        elif cam.name() in object_list:
            object_list.remove(cam.name())

    return object_list

    #object_set = set(object_list)
    #return (object_set - (object_set & set(MAYA_CAMERAS)))


def port(port=":7720", close=False, sourceType="python", **kwargs):

    if not port.startswith(':'):
        port = ''.join([':', port])

    if not close:
        kwargs["noreturn"] = True
        kwargs["echoOutput"] = True
    else:
        kwargs["close"] = True

    try:
        pm.commandPort(name=port, **kwargs)
    except RuntimeError as e:
        print(e)


def visibility_override(outlineFilter=None, override=True, visible=True):

    sel = pm.ls(sl=True) if outlineFilter is None else pm.ls(outlineFilter)

    for node in sel:

        if override:
            pm.editRenderLayerAdjustment(node.visibility)
        else:
            pm.editRenderLayerAdjustment(node.visibility, remove=True)

        if visible:
            node.show()
        else:
            node.hide()


def deferNonInteractive(func, *args, **kwargs):
    import maya.utils
    import maya.OpenMaya
    if maya.OpenMaya.MGlobal.mayaState() == maya.OpenMaya.MGlobal.kInteractive:
        maya.utils.executeDeferred(func, *args, **kwargs)
    else:
        print("Execution of interactive function {0} was silenced")


def try_import(func, *args, **kwargs):
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except ImportError as e:
            print(''.join("Chaoswarbears: ", e))
    return wrapper
