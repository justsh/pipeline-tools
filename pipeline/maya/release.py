# release.py: This script is for publishing assets to the release branch of files in dropbox
#
# Version 9 -- Last edited by Justin
#
# NEEDS TO BE DONE
# Correctness checking
# More error checking
# Callbacks for dropdown menu
# Connect guessing to GUI
#
# FUTURE IMPROVEMENTS
# Automatically set the project correctly by searching the file system.
# If the file isnt named perfectly, make a guess
# Check if the file was updated by someone else on dropbox and make a new version
# Create a non-maya version of the script that allows for non maya files
#
# PARTIALLY IMPLEMENTED
# Non-maya files
#
# TUTORIALS
#	http://kickstand.tv/den/?p=26
#	http://cgkit.sourceforge.net/maya_tutorials/intro/
#   http://www.rtrowbridge.com/blog/2010/02/maya-python-ui-example/
#
# RANDOM
# cmds.fileBrowserDialog(wt = 'title', m=0)
#

import maya.cmds as cmds
import os
import shutil
from functools import partial
from collections import namedtuple
from time import gmtime, strftime

#############################


def enum(*sequential, **named):
    """ Auto enumerated enums """
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

# Declared like a Class, acts like a dict, runs like a tuple
AssetType = namedtuple('AssetType',
                       'name topLevelFolder pipelineFolder guiText')
FilePieces = namedtuple('FilePieces', 'name shotNum type extension')


class ReleaseWindow():
    """ Maya Window Class """

    def __init__(self):
        self.window = None
        self.name = 'publish'
        self.title = 'Team A: Publisher'
        self.iconName = 'Publish'
        self.workspaceName = 'Team A'

        # Path variables
        self.workspacePath = ''
        self.filePath = ''
        self.newFilePath = ''

        # Feedback
        self.errors = []
        self.output = '\n'

        # Modes
        self.modes = enum('UPDATING', 'NEW')
        self.mode = self.modes.UPDATING

        # Type Guessing
        self.assetType = None
        self.isShotFile = False
        self.filePieces = None

        # GUI References
        self.formCol = ''
        self.assetsCol = ''
        self.feedbackCol = ''
        self.buttonRow = ''
        self.releaseBtn = ''

        self.setupGUI()

    def doRelease(self, oldPath=None, newPath=None, fileFormat="mayaAscii", *args):
        print('Releasing...')
        print('This will copy ' + oldPath + ' to ' + newPath)

        # Generate the needed file structure as necessary
        if not os.path.exists(newPath):
            os.makedirs(newPath)

        # Non maya files should be copied with shutil.copy2
        if not "mayaAscii" in fileFormat:
            shutil.copy2(oldPath, newPath) # Copy with metadata
        else:
            cmds.file(rename=newPath) # Sets the new file path. Maya is dumb.
            cmds.file(s=True, type=fileFormat, wcn=False) # Save the file

    def makeNewDir(self, newPath):
        if not os.path.exists(newPath):
            os.makedirs(newPath)

    def refreshPaths(self, *args):
        self.workspacePath = cmds.workspace(q=True, rootDirectory=True)
        self.filePath = cmds.file(q=True, expandName=True)

    def makeAssetList(self, *args):

        path = os.path.join(self.workspacePath, self.assetType.topLevelFolder)
        assets = os.listdir(path)

        # TODO LOW PRIORITY: Code to remove template assets (e.g. "_assetName")
        return assets

    def splitFilePath(self, *args):
        # split the file path and file name to extract naming and types
        filename = os.path.split(self.filePath)[1]  # 0 = dirpath, 1 = filename.ext
        filename = os.path.splitext(filename)  # 0 = file's name, 1 = file's .ext

        pieces = filename[0].split('_')

        if(not pieces[0]=='shot'):
            self.isShotFile = False
            self.filePieces = FilePieces(pieces[0], 0, pieces[1], filename[1])
        else:
            self.isShotFile = True
            self.filePieces = FilePieces('_'.join([pieces[0], pieces[1]]),
                                         pieces[1],
                                         pieces[2],
                                         filename[1])


    def detectAssetPath(self, *args):

        detectedType = self.filePieces.type

        if detectedType not in release_types:
            self.errors.append('Asset/Shot type not recognized')
            self.mode = self.modes.NEW
            return False

        self.assetType = release_types[detectedType]

        expectedFolder = (self.filePieces.name if not
            self.isShotFile else self.filePieces.shotNum)

        if (expectedFolder not in self.makeAssetList()):
            self.errors.append('Asset name / Shot Number not recognized')
            self.mode = self.modes.NEW
            return False

        return True

    def makeNewFilePath(self, *args):

        filename = ''.join([self.filePieces.name,
                            '_', self.assetType.name,
                            self.filePieces.extension])

        fullpath = os.path.join(self.workspacePath,
                                self.assetType.topLevelFolder,
                                self.filePieces.name,
                                self.assetType.pipelineFolder,
                                '')

        self.newFilePath = os.path.normpath(fullpath + filename)

        return self.newFilePath

    def doDeleteCtrl(self, controlName, *args):
        if cmds.control(controlName, exists=True):
            cmds.deleteUI(controlName, control=True)

    def onModeChange(self, mode, controlName, *args):
        self.mode = mode
        self.doDeleteCtrl(controlName)
        self.updateGUI()

    def clearOutput(self, *args):
        self.output = '\n'

    def closeGUI(self, *args):
        if cmds.window(self.name, exists=True):
            cmds.deleteUI(self.name, window=True)

    def setupGUI(self, *args):
        print("Creating GUI for releasing the file.")

        self.closeGUI()

        self.window = cmds.window(self.name,
                                  title=self.title,
                                  iconName=self.iconName,
                                  widthHeight=[500, 300],
                                  resizeToFitChildren=True)

        # The main column
        self.formCol = cmds.columnLayout(adjustableColumn=True,
                                         columnAttach=('both', 10),
                                         rowSpacing=10)

        # The header with toggle buttons for the mode
        cmds.rowLayout(numberOfColumns=2)
        releaseMode = cmds.iconTextRadioCollection('modeRadioCollection')
        cmds.iconTextRadioButton(st='textOnly',
                                 l='Update Asset',
                                 width=120,
                                 select=True,
                                 onCommand=partial(self.onModeChange,
                                                   self.modes.UPDATING,
                                                   'assetOptions'))

        cmds.iconTextRadioButton(st='textOnly',
                                 l='New Asset',
                                 width=100,
                                 onCommand=partial(self.onModeChange,
                                                   self.modes.NEW,
                                                   'assetOptions'))
        cmds.setParent('..')
        cmds.separator()

        # The options and dropdowns column (does not auto-size child controls)
        self.typesCol = cmds.columnLayout(adjustableColumn=False,
                                          columnAttach=('both', 10),
                                          rowSpacing=10)

        typeOptions = cmds.optionMenu('typeOptions',
                                      label='Asset Type',
                                      width=200,
                                      height=24)
        for t in asset_list:
            cmds.menuItem('tOpt' + t.guiText, label=t.guiText)

        cmds.setParent('..')

        # The options and dropdowns column (does not auto-size child controls)
        self.assetsCol = cmds.columnLayout(adjustableColumn=False,
                                           columnAttach=('both', 10),
                                           rowSpacing=10)

        cmds.setParent('..')

        # The errors column (does auto-size child controls)
        cmds.text('')

        cmds.columnLayout(adjustableColumn=True,
                          columnAttach=('both', 10),
                          rowSpacing=10)

        cmds.text('taglineTxt', label='', align='left')
        cmds.text('dirpathTxt', label='', align='left', fn='boldLabelFont')

        cmds.scrollLayout(horizontalScrollBarThickness=0,
                          verticalScrollBarThickness=16,
                          height=200)

        self.feedbackCol = cmds.columnLayout(adjustableColumn=True,
                                             columnAttach=('both', 10),
                                             rowSpacing=10)

        cmds.text(label='\nErrors:', align='left', fn='boldLabelFont')
        cmds.text('outputTxt', label='', align='left', wordWrap=True)

        cmds.setParent(self.formCol)

        cmds.rowLayout(numberOfColumns=3,
                       adjustableColumn=3,
                       columnAttach=[[1, 'left', 10],
                                     [2, 'left', 6],
                                     [3, 'both', 10]])

        cmds.button(label='Clear', command=self.clearOutput, width=60)
        cmds.button(label='Close', command=self.closeGUI, width=60)

        self.releaseBtn = cmds.button(label='Release',
                                      command=partial(self.doRelease,
                                                      self.filePath,
                                                      self.newFilePath),
                                      enable=False)

        self.updateGUI()
        cmds.showWindow(self.window)

        return self.window  # courtesy return

    def updateGUI(self, *args):

        self.refreshPaths()
        self.splitFilePath()
        self.detectAssetPath()
        self.makeNewFilePath()  # IMPORTANT

        cmds.setParent(self.assetsCol)

        if (not cmds.control('assetOptions', exists=True)):
            if(self.mode == self.modes.UPDATING):
                cmds.optionMenu('assetOptions',
                                label="Asset Name",
                                width=200,
                                height=24)
                for a in self.makeAssetList():
                    cmds.menuItem('aOpt_' + a, label=a)
            elif (self.mode == self.modes.NEW):
                cmds.textField('assetOptions',
                               text=self.filePieces.name,
                               width=200,
                               height=24)

        cmds.text('taglineTxt', label='\nThis file will be published under:', edit=True)
        cmds.text('dirpathTxt', label=self.newFilePath, edit=True)

        cmds.setParent(self.feedbackCol)

        # Check if the project path set correctly
        if (self.workspaceName not in self.workspacePath):
            self.errors.append('Project is not set to Team A')

        # Check if the naming convention matches a pre-existing directory
        if (os.path.lexists(self.newFilePath) == False):

            err_nodir = ("Filename is trying to follow a directory that doesn\'t "
                        "exist.\nMake sure correct naming convention is used.\n"
                        "[fileDescrip_department.ma] or [fileDescrip_department_####.ma]")
            print(err_nodir)
            self.errors.append(err_nodir)

        if(not self.errors):
            self.clearOutput()
        else:
            self.output += strftime("%m-%d %H:%M:%S\n", gmtime())
            for s in self.errors:
                self.output += (s + '\n')
            self.output += ('-' * 76 + '\n\n')

        cmds.text('outputTxt', label=self.output, edit=True)

        cmds.setParent(self.buttonRow)

        cmds.button(self.releaseBtn,
                    enable=(not self.errors),
                    edit=True)

        # Close up shop
        self.errors = []
        return

##################### RUN ######################

release_types = []
asset_list = [
         AssetType('model', '0_Assets', '0_model', "Modeling"),
         AssetType('surf', '0_Assets', '1_surfacing', "Surfacing"),
         AssetType('rig', '0_Assets', '2_rig', "Rigging"),
         AssetType('layout', '1_Shots', '3_layout', "Layout"),
         AssetType('anim', '1_Shots', '4_animation', "Animation"),
         AssetType('lighting', '1_Shots', '5_lighting', "Lighting"),
         AssetType('fx', '1_Shots', '6_fx', "Effects (FX)"),
         AssetType('comp', '1_Shots', '7_comp', "Compositing"),
         AssetType('tx', '3_SurfacingLibrary', '', "Library: Surfacing"),
         AssetType('set', '3_LayoutLibrary', '', "Library: Layout"),
         AssetType('fxlib', '3_FXLibrary', '', "Library: FX")
        ]


for t in asset_list:
    release_types.append([t.name, t])

release_types = dict(release_types)  # IMPORTANT

rwin = ReleaseWindow()

############## Create a Shelf Command ###############


def createShelfButton():
    if not cmds.shelfButton('release_command', exists=True):
        cmds.shelfButton('release_command',
                        commandRepeatable=True,
                        i="pythonFamily.png",
                        label="Release Asset",
                        annotation="Release Asset",
                        command="import release",
                        imageOverlayLabel="Pub.",
                        parent="General")

createShelfButton()
