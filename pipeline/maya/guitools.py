'''
Created on Feb 19, 2013

@author: Justin Sheehy
'''


import pymel.core as pm


class MayaMenu(object):
    """
    Creates Maya menus from a dictionary
    """

    def __init__(self, name, label):
        self.name = name
        self.label = label
        self.location = ""

    @classmethod
    def __menu_from_dict(cls, mdict):
        for key, value in mdict.items():

            if "_submenu" in key:
                # submenu is a dictionary comprising a menu
                lbl = value["label"]
                item_name = ''.join(lbl.split())
                item_name = item_name[0].lower() + item_name[1:]

                pm.menuItem(item_name,
                            subMenu=True,
                            label=lbl,
                            tearOff=True)
                cls.__menu_from_dict(value)

                pm.setParent(upLevel=True, menu=True)
            elif "_items" in key:
                # menu items is a list of dictionaries
                for item in value:
                    if "divider" in item:
                        pm.menuItem(divider=item["divider"])
                    else:
                        pm.menuItem(**item)

    @classmethod
    def __menu_from_fs(cls, rootdir):
        import os

        def __menuItems(path, file_names):
            for name in file_names:
                if "divider" in name:
                    pm.menuItem(divider=True)
                else:
                    filepath = os.path.join(path, name)
                    label = os.path.splitext(name)[0]
                    source = ""
                    with open(filepath) as f:
                        source = ''.join(f.readlines())
                    pm.menuItem(label=label, command=source)

        tree = os.walk(rootdir)
        path, dirnames, files = next(tree)
        __menuItems(path, files)
        pm.setParent(upLevel=True, menu=True)

        for path, dirnames, files in tree:

            root = os.path.basename(path)
            words = [w.capitalize() for w in root.split()]
            mi_id = ''.join([words[0].lower(), ''.join(words[1:])])
            mi_name = ' '.join(words)

            pm.menuItem(mi_id,
                        subMenu=True,
                        label=mi_name,
                        tearOff=True)

            __menuItems(path, files)
            pm.setParent(upLevel=True, menu=True)

    def buildUI(self, template):

        try:
            gMainWindow = pm.melGlobals['gMainWindow']
        except KeyError as ke:
            pm.displayError(ke + " did not yet exist at time of request")

        if pm.menu(self.name, exists=True):
            # if the menu exists, we want to delete the buttons, this is to
            # prevent a problem with buttons already existing on the menu.
            pm.menu(self.name, edit=True, deleteAllItems=True)
        else:
            pm.setParent(gMainWindow)
            self.location = pm.menu(self.name,
                                    label=self.label,
                                    tearOff=True)

        pm.setParent(self.name, menu=True)

        # TODO: Find resonable test to choose between filesystem
        # and OrderedDict based methods
        try:
            self.__menu_from_fs(template)
        except:
            raise


class MayaShelf(object):
    """
    Original install_shelf by Tom Craigen
    """

    def __init__(self, name, label):
        self.name = name
        self.label = label

    def buildUI(self, shelf_button_dict):

        dynamicshelf = None
        gShelfTopLevel = pm.melGlobals['gShelfTopLevel']

        if not gShelfTopLevel:
            raise RuntimeError("Could not get top level shelf")

        if pm.shelfLayout(self.name, exists=True):
            # if the shelf exists, we want to delete the buttons, this is to
            # prevent a problem with buttons already existing on the shelf.

            dynamicshelf = self.name

            shelfbuttons = pm.shelfLayout(self.name, q=True, childArray=True)

            if shelfbuttons:
                for button in shelfbuttons:
                    pm.deleteUI(button)

        else:
            dynamicshelf = pm.shelfLayout(self.name, parent=gShelfTopLevel)

        for button_dict in shelf_button_dict:
            button_dict['parent'] = dynamicshelf
            button_dict['width'] = 34
            button_dict['height'] = 34
            button_dict['overlayLabelBackColor'] = [0.0, 0.0, 0.0, 0.8]

            name = button_dict['label']

            img_default = ("pythonFamily.png"
                           if not "spacer" in name
                           else "vacantCell.png")

            button_dict.setdefault("image", img_default)

            # Create the buttons
            pm.shelfButton(name, **button_dict)

    def move(self, index):
        try:
            gShelfTopLevel = pm.getMelGlobal("string", "gShelfTopLevel")
            tabs = pm.shelfTabLayout(gShelfTopLevel, query=True, childArray=True)

            # Maya counts shelf children with a 1-based rather than 0-based index
            cwst = next((tab, i+1) for i, tab in enumerate(tabs) if tab.endswith(self.label))
            print(cwst)

            # Move the tab and select it
            pm.shelfTabLayout(gShelfTopLevel, edit=True, moveTab=(cwst[1], index))
            pm.shelfTabLayout(gShelfTopLevel, edit=True, selectTabIndex=index)

            # Reload the styling for the current tab
            styler = lambda n, **kwargs: pm.shelfButton(n, edit=True, **kwargs)
            style = {"width": 34, "height": 34}
            self.iter(self.name, styler, **style)
        except Exception as e:
            print(e)

    @staticmethod
    def iter(name, func, *args, **kwargs):
        shelfbuttons = pm.shelfLayout(name, query=True, childArray=True)

        for button in shelfbuttons:
            func(button, **kwargs)


class MayaHUD(object):

    def __init__(self, name, settings):
        self.name = name
        self.hud_id = -1
        self.settings = settings

        if not name.startswith("HUD"):
            pm.warning("By convention, HUD names are Title Case and begin with 'HUD'")

    def do(self, **kwargs):
        pm.headsUpDisplay(self.name, **kwargs)

    def toggle(self):
        visibility = pm.headsUpDisplay(self.name, query=True, visible=True)
        pm.headsUpDisplay(self.name, visible=(not visibility))

    def show(self):
        pm.headsUpDisplay(self.name, visible=True)

    def hide(self):
        pm.headsUpDisplay(self.name, visible=False)

    def refresh(self):
        pm.headsUpDisplay(self.name, refresh=True)

    def buildUI(self, next_free_block=True, force=True, *args):

        if force:
            self.delete()

        if next_free_block:
            section = self.settings.get("section", 0)
            self.settings["block"] = pm.headsUpDisplay(nextFreeBlock=section)

        self.hud_id = pm.headsUpDisplay(self.name, **self.settings)

    def delete(self):
        if pm.headsUpDisplay(self.name, exists=True):
            pm.headsUpDisplay(self.name, remove=True)
            self.hud_id = -1


class SpacerUI(object):

    def __init__(self, lbl="spacer", start_at=0, step=1):
        self.id = start_at
        self.label = lbl
        self.step = step

    def spacer(self, *args):
        spacer_lbl = self.label + str(self.id)
        self.id += self.step
        return spacer_lbl

    def reset(self):
        self.id = 0
