from . import mayautils
import pymel.core as pm
import os
import json

# TODO: Check against Vector.zero
# TODO: Write or look for locators to place files

normalized_join = lambda *args: os.path.normpath(os.path.join(*args))


def export_transforms(transforms_list, transforms_filepath, transformMatrix=False):

    # if the selection is empty, raise an error
    if not transforms_list:
        raise RuntimeError("Selection list was empty.")

    pm.select(transforms_list, replace=True)

    transforms = []

    for t in transforms_list:
        if transformMatrix:
            transforms.append(t.getTransformation().tolist())
            #transforms.append(t.getMatrix(worldSpace=True).tolist())
            #transforms.append(t.getMatrix(objectSpace=True).tolist())
        else:
            transforms.append({
                              "rotation": list(t.getRotation()),
                              "translation": list(t.getTranslation()),
                              "scale": list(t.getScale())
                              })

    with open(transforms_filepath, "w") as f:
        json.dump(transforms, f, indent=2)

    output_display = "Saved {count} transform(s) to {path}"
    output_display = output_display.format(count=len(transforms),
                                           path=transforms_filepath)
    pm.displayInfo(output_display)

    pm.confirmDialog(title="Export Transforms",
                     message=output_display,
                     button=["OK"],
                     defaultButton='OK')


def populate_transforms(refdict, transforms_filepath, transformMatrix=False):

    mayautils.set_scene_units()

    # Empty list for holding our JSON data
    transforms = None
    new_references = []

    with open(transforms_filepath, "r") as f:
        transforms = json.load(f)

    if not transforms:
        raise RuntimeError("Transformation data not found")

    for i, matrix in enumerate(transforms):

        if "namespace_tpl" in refdict:
            requested_namespace = refdict["namespace_tpl"].format(i+1)
        else:
            requested_namespace = refdict["namespace"]

        new_nodes = pm.createReference(refdict["filepath"],
                                       namespace=requested_namespace,
                                       sharedNodes="shadingNetworks",
                                       returnNewNodes=True)
        # Use next() so that we only iterate over the current new reference's nodes until we
        # find the first transform node that is also an assembly
        transform_node = next((node for node in new_nodes
                              if node.nodeType() == "transform"
                              and pm.ls(node.name(), assemblies=True)), None)

        if not transform_node:
            pm.displayWarning("Transform node not accessible on node."
                              " Iteration: {0:04d}".format(i))
            continue
        else:
            new_references.append(transform_node.name())

        if transformMatrix:
            transform_node.setTransformation(matrix)
        else:
            transform_node.setTranslation(matrix["translation"])
            transform_node.setRotation(matrix["rotation"])
            transform_node.setScale(matrix["scale"])

    pm.select(new_references, replace=True, hierarchy=False)

    group_as = ("imported_transforms" if not "group_as" in refdict
                else refdict["group_as"])

    pm.group(name=group_as, world=True, relative=True)

    output_display = "Loaded {count} transform(s) from {path}"
    output_display = output_display.format(count=len(transforms),
                                           path=transforms_filepath)
    pm.displayInfo(output_display)

    pm.confirmDialog(title="Populate Transforms",
                     message=output_display,
                     button=["OK"],
                     defaultButton='OK')
