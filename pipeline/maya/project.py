import pymel.core as pm
from .mayautils import mprint
from ..types.ordereddict import OrderedDict
import os


normalize_join = lambda *args: os.path.normpath(os.path.join(*args))


class MayaProject(object):

    def __init__(self, name, path, onValidate=None, register=True):
        self.name = name
        self.path = path

        self.callbacks = OrderedDict()

        self.WorkspaceChangedScriptJob = None
        self.onValidate = (onValidate if onValidate is not None
                           else self.validate)

        if register:
            self.register()

    def __run_once(self, key, func):
        def run_once_wrapper(*args):
            func()
            self.remove_callback(key)
        return run_once_wrapper

    def register(self):
        # workspaceChanged is called when a scene is opened in
        # an existing Maya Session
        # This was causing validate() to be called twice
        #
        # There are only two (known) cases where workspaceChanged and
        # SceneOpened do not overlap
        # 1) workspaceChanged does not fire when a Maya Session is
        #    first started *iff* the scene is untitled
        # 2) SceneOpened will fire upon opening the Open File dialog,
        #    whereas workspaceChanged does not fire until after the new scene
        #    is loaded

        self.WorkspaceChangedScriptJob = pm.scriptJob(
            event=["workspaceChanged", self.onValidate], protected=True)

    def unregister(self):
        pm.scriptJob(kill=self.WorkspaceChangedScriptJob, force=True)

    def add_callback(self, key, func, run_once=False):
        if callable(func):
            if not run_once:
                self.callbacks[key] = func
            else:
                self.callbacks[key] = self.__run_once(key, func)

        else:
            pm.displayError("Function is not callable: " + func)

    def remove_callback(self, key):
        try:
            return self.callbacks.pop(key)
        except KeyError:
            pm.displayError("Key \"{0}\" does not match a registered "
                            "callback".format(key))

    def root(self):
        return os.path.basename(self.path)

    def validate(self):

        open_scene_path = pm.sceneName()
        if not open_scene_path:
            # The file has not been saved
            # Wait until the file is saved to try again (also, a new scene)
            pm.scriptJob(runOnce=True, event=["SceneSaved", self.validate])

            mprint("MayaProject: Not enough information to evaluate workspace"
                   " settings (File has never been saved)")
            return False
        elif pm.internalVar(userTmpDir=True) in open_scene_path:
            # File has probably crashed, just ignore for now
            return

        workspace_path = os.path.normpath(pm.Workspace().getPath())
        workspace_root = os.path.basename(pm.Workspace().getName())

        cwb_file = self.root() in open_scene_path
        cwb_project = self.root() == workspace_root

        if cwb_file and cwb_project:
            # Project is set and file is in the project
            mprint("MayaProject: Project location and current file match "
                   "expected settings. Sleeping until next file change.")

            for cb in self.callbacks.itervalues():
                try:
                    cb()
                except Exception as e:
                    # TODO: Get stack trace of calls
                    pm.displayError("{0} project callback \"{1}\" failed "
                                    "to run".format(self.name, cb))
                    print(e)

            return True

        elif cwb_file and not cwb_project:
            # Project is not set but the file is within the expected location
            # offer to set project to chaoswarbears
            output_str = ("You appear to be editing {0} project files "
                          "without your project set properly.\n"
                          "You are currently in project: \n\n{1} ({2})\n\n"
                          "You should be in project \n\n{0} ({3})\n\n"
                          "You can change your project from "
                          "File Menu > Set Project... \n"
                          "How would you like to proceed?")

            title_str = ' '.join([self.name, "Project is not set"])
            output_str = output_str.format(self.name,
                                           workspace_root,
                                           workspace_path,
                                           self.path)

            ignore = "Ignore"
            set_to_expected = ' '.join(["Autoset to", self.name, "Project"])
            open_set_project_ui = "Open \"Set Project...\""

            result = pm.confirmDialog(title=title_str,
                                      message=output_str,
                                      messageAlign="left",
                                      button=[ignore,
                                              set_to_expected,
                                              open_set_project_ui],
                                      defaultButton=open_set_project_ui,
                                      cancelButton=ignore,
                                      dismissString=ignore)

            if result == set_to_expected:
                pm.Workspace.open(self.path)
                pm.openFile(pm.sceneName(), force=True)
            elif result == open_set_project_ui:
                pm.mel.evalDeferred("SetProject")

            return

        elif cwb_project and not cwb_file:
            # Project is set but the file is outside of the project
            # We should

            title_str = "Which project did you mean to use?"
            output_str = ("Your project is set to \n\n{0} ({1})\n\n"
                          "but your current file is not below that path.\n"
                          "Your current file is here: \n\n{2}\n\n"
                          "Perhaps this is a personal project?\n"
                          "If you do not care which project you use, it is "
                          "recommended you switch to the Default project\n\n"
                          "You can change your project from "
                          "File Menu > Set Project... \n"
                          "How would you like to proceed?")

            output_str = output_str.format(self.name,
                                           self.path,
                                           open_scene_path)

            ignore = "Ignore"
            set_to_default = "Autoset to Default Project"
            open_set_project_ui = "Open \"Set Project...\""

            result = pm.confirmDialog(title=title_str,
                                      message=output_str,
                                      messageAlign="left",
                                      button=[ignore,
                                              set_to_default,
                                              open_set_project_ui],
                                      defaultButton=open_set_project_ui,
                                      cancelButton=ignore,
                                      dismissString=ignore)

            if result == set_to_default:
                user_workspace_dir = pm.internalVar(userWorkspaceDir=True)
                default_project = normalize_join(user_workspace_dir, "default")
                pm.Workspace().open(default_project)
                pm.openFile(pm.sceneName(), force=True)
            elif result == open_set_project_ui:
                pm.mel.evalDeferred("SetProject")

            return

        return None
