import os.path
import pymel.core as pm
from . import mayautils
from .mayautils import mprint


class SaveCommands(object):

    widgets = {}
    widgets["backup"] = {
        "menu": "",
        "default": "SaveBackupVersion",
        "attrs": {
                   "label": "Save Backup Version",
                   "insertAfter": "saveAsItem",
                   "command": None
        }
    }

    widgets["as_copy"] = {
        "menu": "",
        "default": "SaveAsCopy",
        "attrs": {
                   "label": "Save As Copy...",
                   "insertAfter": "saveAsItem",
                   "command": None
        }
    }

    def __init__(self):
        cls = SaveCommands

        cls.widgets["as_copy"]["attrs"]["command"] = cls.promptSaveAsCopy
        cls.widgets["backup"]["attrs"]["command"] = cls.promptSaveBackupVersion

    @staticmethod
    def save_as_copy(new_filepath, file_format="mayaAscii"):
        """
        Given a new file path, perform a save as on the current file to the
        new file path and then save back to the new file
        """

        # TODO: Add a file_format parameter

        norm_equals = lambda a, b: os.path.normpath(a) == os.path.normpath(b)

        # Note: trying to call normpath on empty string results in '.'
        # This would cause "not current_filepath" to fail, so instead just do
        # the normalization in the comparison
        current_filepath = pm.sceneName()

        if not current_filepath:
            # File has never been saved, apply name (will act like normal save)
            pm.renameFile(new_filepath)
            mprint("Result: ", new_filepath)

        elif not norm_equals(current_filepath, new_filepath):
            # File has been given a new name

            # Save as the new name
            pm.renameFile(new_filepath)
            pm.saveFile(type=file_format, force=True)
            mprint("Copied to ", new_filepath,
                   ". Still editing ", os.path.basename(current_filepath))

            # Then come back into the current name
            pm.renameFile(current_filepath)

        else:
            # The requested new name is not different than the current
            # Fall through to normal save
            mprint("Result: ", new_filepath)

        pm.saveFile(type=file_format, force=True)

    @classmethod
    def save_backup_version(cls, filepath, *args):
        """
        Save the current scene with a padded increment number
        Currently only saves and checks the same directory
        """

        # TODO: Add destination_dir parameter
        # TODO: Add padding parameter

        filename = os.path.basename(filepath)
        filename_no_ext = os.path.splitext(filename)[0]
        dir_path = os.path.dirname(filepath)

        maya_files = [s for s in os.listdir(dir_path) if s.endswith(".ma")]
        versioned_files = [f for f in maya_files
                           if f.startswith(filename_no_ext + '_')]

        versioned_file_count = len(versioned_files) + 1
        new_filename = "{0}_{1:03d}{2}".format(filename_no_ext,
                                               versioned_file_count, ".ma")
        new_filepath = os.path.join(dir_path, new_filename)

        cls.save_as_copy(new_filepath)

    @classmethod
    def promptSaveAsCopy(cls, *args):
        """
        Prompt for the path to save a copy
        """

        # Returns a list of the files selected in the dialog.
        # Only one path will be returned in "save" mode,
        # but it will still be in a list

        filepath = pm.sceneName()

        if not filepath:
            return mayautils.save_untitled()

        fileFilters = ';;'.join(["Maya Files (*.ma *.mb)",
                                 "Maya ASCII (*.ma)",
                                 "Maya Binary (*.mb)",
                                 "All Files (*.*)"])

        result = pm.fileDialog2(caption="Save A Copy",
                                fileFilter=fileFilters,
                                selectFileFilter="Maya ASCII")

        if (result is not None):
            new_filename = result[0]
            cls.save_as_copy(new_filename)
        else:
            pm.displayWarning("\"Save a Copy\" exited without saving.")

    @classmethod
    def promptSaveBackupVersion(cls, *args):

        filepath = pm.sceneName()

        if not filepath:
            return mayautils.save_untitled()

        cls.save_backup_version(filepath)

    @classmethod
    def buildUI(cls, force=False):
        if force:
            cls.deleteUI()

        def _build(widget, name, **kwargs):
            if not widget:
                if not pm.menuItem(widget, exists=True):
                    return cls._add_fileMenu_item(name, **kwargs)

        for key, value in cls.widgets.items():
            try:
                cls.widgets[key]["menu"] = _build(value["menu"],
                                                  value["default"],
                                                  **value["attrs"])
            except RuntimeError as e:
                print(e)

    @classmethod
    def deleteUI(cls, *args):
        for key, value in cls.widgets.items():
            cls._delete_menuitem(value["default"])
            cls.widgets[key]["menu"] = ""

    @classmethod
    def _add_fileMenu_item(cls, name, *args, **kwargs):
        """
        Adds a menu item to the fileMenu after a specified existing menu item
        insertAfter is passed as a keyword argument to pm.menuItem
        """
        # NOTE: Appending to Menus
        # https://groups.google.com/
        #        forum/#!topic/python_inside_maya/eLB5VbU8i6o/discussion
        #
        # NOTE: saveAsItem
        # http://nccastaff.bournemouth.ac.uk/
        #        jmacey/RobTheBloke/www/research/maya/version_control.htm

        # fileMenu = pm.melGlobals['$gMainFileMenu'] will fail on load
        fileMenu = pm.getMelGlobal('string', 'gMainFileMenu')

        try:
            pm.mel.eval("checkMainFileMenu")
        except:
            pm.mel.eval("buildFileMenu")

        pm.mel.eval("buildFileMenu")
        pm.menu(fileMenu, edit=True, tearOff=False)
        menu_item = pm.menuItem(name, parent=fileMenu, **kwargs)

        mprint("Result: \"{0}\" as {1}".format(kwargs['label'], menu_item))

        return menu_item

    @staticmethod
    def _delete_menuitem(item, *args):
        if pm.menuItem(item, exists=True):
            pm.deleteUI(item, menuItem=True)
