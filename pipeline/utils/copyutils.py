import os
import errno
import shutil
from . import pathutils


normalize_join = lambda *args: os.path.normpath(os.path.join(*args))


def log(*args):
    print(args)


#http://stackoverflow.com/a/5032238
#http://stackoverflow.com/a/2656405
def make_sure_path_exists(path):

    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def recursive_delete(d):
    try:
        shutil.rmtree(d, onerror=pathutils.onerror)
    except IOError as e:
        print(e)


def cptree(root_srcdir,
           root_dstdir,
           action,
           filter_by=None,
           verbose=True,
           **kwargs):

    if not callable(action):
        raise RuntimeError("Paramater 'action' must be callable")

    paths = [normalize_join(srcdir, f)
             for srcdir, dirs, files in os.walk(root_srcdir)
             for f in files]

    if callable(filter_by):
        paths = filter(filter_by, paths)

    copied = 0
    for path in paths:
        srcdir, src_filename = os.path.split(path)
        dstdir = srcdir.replace(root_srcdir, root_dstdir)

        make_sure_path_exists(path)

        r = action(path, dstdir, **kwargs)

        if r:
            copied = copied + 1

    return copied


def copy(src_filepath, dstdir, **kwargs):
    verbose = kwargs.get("verbose", False)

    if verbose:
        src_filename = os.path.basename(src_filepath)
        log("copying {0} to {1}".format(src_filename, dstdir))

    try:
        shutil.copy(src_filepath, dstdir)
        return True
    except IOError as e:
        log(e)
        return False


def move(src_filepath, dstdir, **kwargs):
    verbose = kwargs.get("verbose", False)

    src_filename = os.path.basename(src_filepath)
    dst_filepath = os.path.join(dstdir, src_filename)

    try:
        os.remove(dst_filepath)
    except OSError:
        log("removing old version")

    if verbose:
        log("moving {0} to {1}".format(src_filename, dstdir))

    try:
        shutil.move(src_filepath, dstdir)
        return True
    except IOError as e:
        log(e)
        return False


def smart_copy(src_filepath, dstdir, **kwargs):
    verbose = kwargs.get("verbose", False)
    threshold = kwargs.get("threshold", 1)

    src_filename = os.path.basename(src_filepath)
    dst_filepath = os.path.join(dstdir, src_filename)

    needs_update = True
    try:
        # If delta is positive and greater than the threshold
        src_modified_time = os.stat(src_filepath).st_mtime
        dst_modified_time = os.stat(dst_filepath).st_mtime
        needs_update = (src_modified_time - dst_modified_time
                        > threshold)
    except OSError:
        needs_update = False

    if needs_update:
        try:
            shutil.copy(src_filepath, dstdir)
            if verbose:
                log("copying newer {0} to {1}".format(src_filename, dstdir))
            return True
        except IOError as e:
            log(e)

    return False


def dry_run(src_filepath, dstdir, **kwargs):
    log(' '.join(["*", src_filepath, "->", dstdir]))
    return True
