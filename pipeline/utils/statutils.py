import sys


def machine():
    """ http://stackoverflow.com/a/11742949 """
    import os
    try:
        return os.uname()[-1]
    except AttributeError:
        if "PROCESSOR_ARCHITEW6432" in os.environ:
            return os.environ.get("PROCESSOR_ARCHITEW6432", '')
        else:
            return os.environ.get('PROCESSOR_ARCHITECTURE', '')


class MemoryCheck(object):
    """Checks memory of a given system"""
    # TODO: the memory check should probably not be called automatically

    def __init__(self):
        # TODO: Make this based on feature sniffing rather than os
        if sys.platform.startswith("darwin"):
            self.memstats = self.darwin_memory()
        elif sys.platform.startswith("win"):
            self.memstats = self.windows_memory()
        elif sys.platform.startswith("linux"):
            self.memstats = self.linux_memory()
        else:
            self.memstats = None
            print("Could not automatically detect supported os")

    def windows_memory(self):
        """
        Uses Windows API to check RAM in this OS
        Most solutions on stackoverflow don't account for 64bit memory

        Adapted from those and from this post, which does
        http://blenderartists.org/forum/showthread.php?205082-Python-get-ram-data&p=1760066&viewfull=1#post1760066

        """
        import ctypes
        from ctypes.wintypes import DWORD

        kernel32 = ctypes.windll.kernel32

        if "64" in machine():
            SIZE_T = ctypes.c_ulonglong

            class MEMORYSTATUSEX(ctypes.Structure):
                _fields_ = [
                    ("dwLength", DWORD),
                    ("dwMemoryLoad", DWORD),
                    ("ullTotalPhys", SIZE_T),
                    ("ullAvailPhys", SIZE_T),
                    ("ullTotalPageFile", SIZE_T),
                    ("ullAvailPageFile", SIZE_T),
                    ("ullTotalVirtual", SIZE_T),
                    ("ullAvailVirtual", SIZE_T),
                    ("sullAvailExtendedVirtual", SIZE_T)
                ]
            mstatus = MEMORYSTATUSEX()
            mstatus.dwLength = ctypes.sizeof(MEMORYSTATUSEX)
            kernel32.GlobalMemoryStatusEx(ctypes.byref(mstatus))

        else:
            SIZE_T = ctypes.c_ulong

            class MEMORYSTATUS(ctypes.Structure):
                _fields_ = [
                    ("dwLength", DWORD),
                    ("dwMemoryLoad", DWORD),
                    ("dwTotalPhys", SIZE_T),
                    ("dwAvailPhys", SIZE_T),
                    ("dwTotalPageFile", SIZE_T),
                    ("dwAvailPageFile", SIZE_T),
                    ("dwTotalVirtual", SIZE_T),
                    ("dwAvailVirtual", SIZE_T),
                ]

            mstatus = MEMORYSTATUS()
            mstatus.dwLength = ctypes.sizeof(MEMORYSTATUS)
            kernel32.GlobalMemoryStatusEx(ctypes.byref(mstatus))

        # Pack return into a dict
        # FIXME: Does not pack
        members = []
        keep = ["Total", "Avail"]

        for s in keep:
            for key, size in mstatus._fields_:
                try:
                    ind = key.index(s)
                except ValueError:
                    continue

                members.append((key[ind:],
                               getattr(mstatus, key)/1024**2))

        return dict(members)

    def linux_memory(self):
        """Returns the RAM of a linux system in megabytes using the free utility"""
        import subprocess

        process = subprocess.Popen("free -m",
                                   shell=True,
                                   stdout=subprocess.PIPE,
                                   )

        stats = {}
        lines = process.communicate()[0].split('\n')
        MNAME, TOTAL, USED, AVAIL = 0, 1, 2, 3

        for line in lines:
            if line.startswith("Mem"):
                values = line.split()
                stats["TotalPhys"] = values[TOTAL]
                stats["AvailPhys"] = values[AVAIL]
            elif line.startswith("Swap"):
                stats["TotalPageFile"] = values[TOTAL]
                stats["AvailPageFile"] = values[AVAIL]

        return stats

    def darwin_memory():
        """Returns the RAM of a linux system in megabytes using the free utility"""
        import subprocess

        """
        OLD but offers more fine-tuning
        Use instead os sysctl's hw.memsize if you need available memory

        vmstat = subprocess.Popen("vm_stat",
                                   shell=True,
                                   stdout=subprocess.PIPE,
                                   )

        stats = {}

        lines = vmstat.communicate()[0].split('\n')[1:6]
        print lines
        values = []
        for line in lines:
            text, value = line.split(':')
            v = value.strip(' .')
            v_mb = int(v) * 4096 / 1024 / 1024
            values.append(v_mb)

        mem_total = sum(values)

        # This (unless the system has reserved an inordinate amount of your
        # RAM, or some has been sectioned off for your safety), should match
        # the value returned by scsctl hw.memsize
        mem_installed = int(ceil(mem_total / 1024.0) * 1024)

        stats["TotalPhys"] = mem_installed
        """

        sysctl = subprocess.Popen("sysctl -n hw.memsize vm.swapusage",
                                  shell=True,
                                  stdout=subprocess.PIPE,
                                  )

        stats = {}
        lines = sysctl.communicate()[0].split('\n')[:-1]

        stats["TotalPhys"] = int(lines[0]) / 1024 ** 2

        swaplines = lines[1].strip().split('  ')

        values = []
        for line in swaplines:
            text, value = line.split('=')
            values.append(value.strip(' M'))

        stats["TotalPageFile"] = values[0]
        stats["AvailPageFile"] = values[1]

        return stats
